import Criterion.Main
import qualified Day01
import qualified Day02
import qualified Day03
import qualified Day04
import qualified Day05
import qualified Day06
import qualified Day07
import qualified Day08
import qualified Day09
import qualified Day10
import qualified Day11
import qualified Day12
import qualified Day13
import qualified Day14
import qualified Day15
import qualified Day16
import qualified Day17

main :: IO ()
main = defaultMain
  [ bgroup "day01"
    [ bench "1" $ nfIO Day01.star1 
    , bench "2" $ nfIO Day01.star2
    ]
  , bgroup "day02"
    [ bench "1" $ nfIO Day02.star1 
    , bench "2" $ nfIO Day02.star2
    ]
  , bgroup "day03"
    [ bench "1" $ nfIO Day03.star1 
    , bench "2" $ nfIO Day03.star2
    ]
  , bgroup "day04"
    [ bench "1" $ nfIO Day04.star1 
    , bench "2" $ nfIO Day04.star2
    ]
  , bgroup "day05"
    [ bench "1" $ nfIO Day05.star1 
    , bench "2" $ nfIO Day05.star2
    ]
  , bgroup "day06"
    [ bench "1" $ nfIO Day06.star1 
    , bench "2" $ nfIO Day06.star2
    ]
  , bgroup "day07"
    [ bench "1" $ nfIO Day07.star1 
    , bench "2" $ nfIO Day07.star2
    ]
  , bgroup "day08"
    [ bench "1" $ nfIO Day08.star1 
    , bench "2" $ nfIO Day08.star2
    ]
  , bgroup "day09"
    [ bench "1" $ nfIO Day09.star1 
    , bench "2" $ nfIO Day09.star2
    ]
  , bgroup "day10"
    [ bench "1" $ nfIO Day10.star1 
    , bench "2" $ nfIO Day10.star2
    ]
  , bgroup "day11"
    [ bench "1" $ nfIO Day11.star1 
    , bench "2" $ nfIO Day11.star2
    ]
  , bgroup "day12"
    [ bench "1" $ nfIO Day12.star1 
    , bench "2" $ nfIO Day12.star2
    ]
  , bgroup "day13"
    [ bench "1" $ nfIO Day13.star1 
    , bench "2" $ nfIO Day13.star2
    ]
  , bgroup "day14"
    [ bench "1" $ nfIO Day14.star1 
    , bench "2" $ nfIO Day14.star2
    ]
  , bgroup "day15"
    [ bench "1" $ nf Day15.runPuzzle 2020 
    , bench "2" $ nf Day15.runPuzzle 30000000
    ]
  , bgroup "day16"
    [ bench "1" $ nfIO Day16.star1
    , bench "2" $ nfIO Day16.star2
    ]
   , bgroup "day17"
    [ bench "1" $ nf (Day17.runCycles Day17.realBoard) 6 
    , bench "2" $ nf (Day17.runCycles4 Day17.realBoard4) 6
    ]
  ]