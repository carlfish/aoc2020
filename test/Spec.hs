import Test.HUnit
import qualified Day01
import qualified Day02
import qualified Day03
import qualified Day04
import qualified Day05
import qualified Day06
import qualified Day07
import qualified Day08
import qualified Day09
import qualified Day10
import qualified Day11
import qualified Day12
import qualified Day13
import qualified Day14
import qualified Day15
import qualified Day16
import qualified Day17


main :: IO Counts
main = runTestTT tests

tests :: Test
tests = test 
  [ "day1_1" ~: assertAnswer Day01.star1 (Just 802011)
  , "day1_2" ~: assertAnswer Day01.star2 (Just 248607374)
  , "day2_1" ~: assertAnswer Day02.star1 458
  , "day2_2" ~: assertAnswer Day02.star2 342
  , "day3_1" ~: assertAnswer Day03.star1 240
  , "day3_2" ~: assertAnswer Day03.star2 2832009600
  , "day4_1" ~: Day04.star1 >>= (@=? 190)
  , "day4_2" ~: Day04.star2 >>= (@=? 121)
  , "day5_1" ~: assertAnswer Day05.star1 (Just 978)
  , "day5_2" ~: assertAnswer Day05.star2 (Just 727)
  , "day6_1" ~: Day06.star1 >>= (@=? 6297)
  , "day6_2" ~: Day06.star2 >>= (@=? 3158)
  , "da77_1" ~: assertAnswer Day07.star1 229
  , "day7_2" ~: assertAnswer Day07.star2 6683
  , "day8_1" ~: Day08.star1 >>= (@=? (Left "Looped 1384"))
  , "day8_2" ~: assertAnswer Day08.star2 761
  , "day9_1" ~: assertAnswer Day09.star1 (Just 31161678)
  , "day9_2" ~: assertAnswer Day09.star2 5453868
  , "day10_1" ~: assertAnswer Day10.star1 1700
  , "day10_2" ~: assertAnswer Day10.star2 12401793332096
  , "day11_1" ~: assertAnswer Day11.star1 2427
  , "day11_2" ~: assertAnswer Day11.star2 2199
  , "day12_1" ~: assertAnswer Day12.star1 2847
  , "day12_2" ~: assertAnswer Day12.star2 29839
  , "day13_1" ~: assertAnswer Day13.star1 (Just 296)
  , "day13_2" ~: assertAnswer Day13.star2 535296695251210  
  , "day14_1" ~: assertAnswer Day14.star1 17481577045893
  , "day14_2" ~: assertAnswer Day14.star2 4160009892257  
  , "day15_1" ~: Day15.star1 @=? 866
  , "day15_2" ~: Day15.star2 @=? 1437692
  , "day16_1" ~: assertAnswer Day16.star1 19240
  , "day16_2" ~: assertAnswer Day16.star2 21095351239483
  , "day17_1" ~: Day17.star1 @=? 382
  , "day17_2" ~: Day17.star2 @=? 2552
  ]

assertAnswer :: (Show a, Eq a) => IO (Either String a) -> a -> Assertion
assertAnswer f v = f >>= ((@=?) (Right v))