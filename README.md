# advent2020

Charles attempts at Advent of Code 2020.

# Notes

I'm not here to play golf. Finding the shortest, cleverest answer isn't as much a priority as writing 
code that, if I come back to it later, still tells me what problem it is solving and how.

All solutions are my own work, but after I've solved a problem myself I do go look at other peoples
solutions, and might bring their good ideas back to my code.