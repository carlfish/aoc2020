module Day06 (star1, star2) where

import qualified Data.Attoparsec.ByteString.Char8 as AP

import           Control.Applicative (Alternative((<|>)))
import           Control.Monad       (join)
import           Data.List           (group, sort)

import           Lib                 (runFileBlankLineSeparated)

newtype Yes = Yes Char 
  deriving (Eq, Ord)
type GroupYesses = [[ Yes ]]

countUniques :: GroupYesses -> Int
countUniques = length . group . sort . join

countUnanimous :: GroupYesses -> Int
countUnanimous as =
  let candidates = length as
  in length (filter ((==) candidates . length) ((group . sort . join) as))

-- Parsers

parseGroup :: AP.Parser GroupYesses
parseGroup = 
  let parseAnswers = AP.many1 (Yes <$> AP.satisfy (AP.inClass "a-z"))
  in AP.many1 (parseAnswers <* (AP.endOfLine <|> AP.endOfInput))

-- Puzzles

runProblem :: ([ GroupYesses ] -> a) -> IO a
runProblem f = runFileBlankLineSeparated "data/day06/0601.txt" parseGroup f

star1 :: IO Int
star1 = runProblem (sum . fmap countUniques)

star2 :: IO Int
star2 = runProblem (sum . fmap countUnanimous)