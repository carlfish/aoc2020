module Day16 (star1, star2) where

import Data.Attoparsec.ByteString.Char8 (Parser)
import qualified Data.Attoparsec.ByteString.Char8 as AP
import Lib (runFile')
import Data.ByteString.Char8 (ByteString, isPrefixOf)
import Control.Monad (join)
import Data.List ((\\))

type Label = ByteString
data Range = Range Int Int
  deriving Eq
data Class = Class Label Range Range
  deriving Eq

type Ticket = [ Int ]
data Doc = Doc [ Class ] Ticket [ Ticket ]

inClass :: Class -> Int -> Bool
inClass (Class _ r1 r2) i = 
  let inRange (Range l h) = i >= l && i <= h 
  in inRange r1 || inRange r2

isValidTicket :: [ Class ] -> Ticket -> Bool
isValidTicket cs t = all (\tt -> any (\c -> inClass c tt) cs) t

matchingClasses :: [ Class ] -> Int -> [ Class ]
matchingClasses cs i = filter (\c -> inClass c i) cs

invalidNumbers :: [ Class ] -> Ticket -> [ Int ]
invalidNumbers cs ts = filter (\t -> all (\c -> not (inClass c t)) cs) ts

resolveUniques :: [[ Class ]] -> [ Class ]
resolveUniques ccs =
  let 
    uniques = head <$> (filter (\cs -> (length cs == 1)) ccs)
    pruned = (\cs -> if (length cs == 1) then cs else cs \\ uniques) <$> ccs
  in 
    if (length uniques) == (length ccs) then uniques
      else resolveUniques pruned
  
classifyTickets :: Doc -> [ Class ]
classifyTickets (Doc classes myTicket nearTickets) = 
  let
    removeImpossibleClasses t ccs = uncurry matchingClasses <$> zip ccs t
    validNearTickets = filter (isValidTicket classes) nearTickets
  in 
    resolveUniques $ foldr
      removeImpossibleClasses 
      (replicate (length myTicket) classes) 
      validNearTickets

fieldsMatching :: (Label -> Bool) -> [ Class ] -> Ticket -> [ Int ]
fieldsMatching f classes ticket =
  fmap snd (filter (\((Class l _ _), _) -> f l) (zip classes ticket))

-- Parser
parseClasses :: Parser [Class]
parseClasses = 
  let
    parseLabel = AP.takeWhile (AP.inClass "a-z ")
    parseRange = Range <$> AP.decimal <* AP.char '-'  <*> AP.decimal
    parseClass = Class <$> parseLabel <* AP.string ": " <*> parseRange <* AP.string " or " <*> parseRange
  in
    AP.sepBy parseClass AP.endOfLine <* AP.endOfLine

parseDoc :: Parser Doc
parseDoc =
  let
    parseTicket = AP.sepBy AP.decimal (AP.char ',')
    parseMyTicket = AP.string "your ticket:" *> AP.endOfLine *> parseTicket <* AP.endOfLine
    parseNearbyTickets = AP.string "nearby tickets:" *> AP.endOfLine *> (AP.sepBy parseTicket AP.endOfLine)
  in
     Doc 
      <$> parseClasses <* AP.endOfLine
      <*> parseMyTicket <* AP.endOfLine 
      <*> parseNearbyTickets

-- Puzzles 

runProblem :: (Doc -> a) -> IO (Either String a)
runProblem = runFile' "data/day16/1601.txt" parseDoc

star1 :: IO (Either String Int)
star1 = runProblem (\(Doc cs _ ts) -> sum (join (invalidNumbers cs <$> ts)))

solveStarTwo :: Doc -> Int
solveStarTwo doc@(Doc _ t _) =
  let
    classification = classifyTickets doc
    matchingFieldValues = fieldsMatching (isPrefixOf "departure") classification t
  in
    product matchingFieldValues

star2 :: IO (Either String Int)
star2 = runProblem solveStarTwo
