module Day10 (star1, star2) where

import qualified Data.Attoparsec.ByteString.Char8 as AP
import Lib (runFile', onePerLine)
import Data.IntMap (fromList, (!))
import Data.List (sort, tails)

countDiffs :: Int -> [ Int ] -> Int
countDiffs d is = 
  foldl 
    (\s (i, j) -> if (j - i == d) then s + 1 else s) 
    0 
    (zip (0 : is) (is ++ [ (maximum is) + 3 ]))

allPaths :: [ Int ] -> Int
allPaths adapters =
  let 
    memoized_pathsFrom i = fromList (zip adapters (countPaths <$> (tails adapters))) ! i

    countPaths [] = undefined
    countPaths (_ : []) = 1
    countPaths (i:is) = foldl (\a next -> if (next - i > 3) then a else a + memoized_pathsFrom next) 0 is
  in
    countPaths (0 : adapters)

runProblem :: ([Int] -> a) -> IO (Either String a)
runProblem = runFile' "data/day10/1001.txt" (onePerLine AP.decimal)

star1 :: IO (Either String Int)
star1 = runProblem ((\is -> (countDiffs 1 is) * (countDiffs 3 is)) . sort)

star2 :: IO (Either String Int)
star2 = runProblem (allPaths . sort)