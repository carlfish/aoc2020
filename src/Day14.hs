module Day14 (star1, star2) where

import qualified Data.Attoparsec.ByteString.Char8 as AP
import           Control.Applicative              (Alternative((<|>)))
import Lib (runFile', onePerLine)
import Data.Word (Word64)
import Data.Bits (setBit, (.&.), (.|.), complement)
import Data.Map (Map, insert, empty)

type Memory = Map Word64 Word64

data MaskPat = MSet | MClear | MIgnore
data Instr 
  = SetMem Word64 Word64
  | SetMask BitMask

data BitMask = BitMask 
  { setBits :: Word64
  , clearBits :: Word64
  , variableBits :: [ Int ]
  }

setSetBit :: Int -> BitMask -> BitMask
setSetBit i b = b { setBits = setBit (setBits b) i}  

setClearBit :: Int -> BitMask -> BitMask
setClearBit i b = b { clearBits = setBit (clearBits b) i}  

clearClearBits :: BitMask -> BitMask
clearClearBits b = b { clearBits = 0 }

addVarBit :: Int -> BitMask -> BitMask
addVarBit i b = b { variableBits = i : (variableBits b) }

setMaskedBit :: MaskPat -> Int -> BitMask -> BitMask
setMaskedBit MSet = setSetBit
setMaskedBit MClear = setClearBit
setMaskedBit MIgnore = addVarBit

applyMask :: BitMask -> Word64 -> Word64
applyMask mask w = (w .|. (setBits mask)) .&. (complement (clearBits mask))

emptyMask :: BitMask
emptyMask = BitMask 0 0 []

runProgram :: [ Instr ] -> Memory
runProgram is =
  let
    runInstr (bm, mem) instr = case instr of
      SetMask nbm  -> (nbm, mem)
      SetMem l i -> (bm, insert l (applyMask bm i) mem)
  in
    snd $ foldl runInstr (emptyMask, empty) is

runProgram2 :: [ Instr ] -> Memory
runProgram2 is =
  let
    runInstr (mask, memory) instr = case instr of
      SetMask nextMask  -> (nextMask, memory)
      SetMem  l i       -> (mask, setAllMaskedMemory l i mask memory)
  in
    snd $ foldl runInstr (emptyMask, empty) is

setAllMaskedMemory :: Word64 -> Word64 -> BitMask -> Memory -> Memory
setAllMaskedMemory addr value initMask initMem =
  let 
    setAllLocs mask [] = insert (applyMask mask addr) value
    setAllLocs mask (vb:vbs) =
      ( setAllLocs (setSetBit   vb mask) vbs 
      . setAllLocs (setClearBit vb mask) vbs
      )
  in
    setAllLocs (clearClearBits initMask) (variableBits initMask) initMem

-- Parser

parseMask :: AP.Parser BitMask
parseMask = 
  let 
    parseBit (c, f) = const f <$> AP.char c
    toBitMask ps = foldr (\(idx, p) b -> setMaskedBit p idx b) 
      emptyMask (zip [0..] (reverse ps))
  in toBitMask <$> AP.many1
    ( AP.choice (fmap parseBit 
        [ ('0', MClear), ('1', MSet), ('X', MIgnore) ])
    )

parseInstr :: AP.Parser Instr
parseInstr = 
  (SetMem  <$> (AP.string "mem[" *> AP.decimal) <*> (AP.string "] = " *> AP.decimal)) <|>
  (SetMask <$> (AP.string "mask = " *> parseMask))

parseInput :: AP.Parser ([ Instr ])
parseInput = onePerLine parseInstr

-- Problem

runProblem :: ([Instr] -> a) -> IO (Either String a)
runProblem = runFile' "data/day14/1401.txt" parseInput

star1 :: IO (Either String Word64)
star1 = runProblem (sum . runProgram)

star2 :: IO (Either String Word64)
star2 = runProblem (sum . runProgram2)