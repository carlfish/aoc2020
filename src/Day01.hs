module Day01 (star1, star2, runNSum, twoThatSumTo) where

import qualified Data.Attoparsec.ByteString.Char8 as AP

import           Control.Applicative ((<|>))
import           Data.Bifunctor      (Bifunctor(bimap))
import           Data.List           (sort)

import           Lib                 (runFile', onePerLine)

twoThatSumTo :: Int -> [ Int ] -> Maybe (Int, Int)
twoThatSumTo i =
  let
    partitioned = (bimap id reverse) . (span (<= (i `div` 2)))
    solve (a : as) (b : bs) = 
      case compare (a + b) i of
        EQ -> Just (a, b)
        LT -> solve as (b : bs)
        GT -> solve (a : as) bs
    solve _ _ = Nothing
  in
    (uncurry solve) . partitioned

threeThatSumTo :: Int -> [ Int ] -> Maybe (Int, Int, Int)
threeThatSumTo _ [] = Nothing
threeThatSumTo i (a : as) = 
  (\(b, c) -> (a, b, c)) <$> (twoThatSumTo (i - a) as) <|> threeThatSumTo i as

nThatSumTo :: Int -> Int -> [ Int ] -> Maybe [ Int ]
nThatSumTo _ _ []     = Nothing
nThatSumTo 0 _ _      = Nothing
nThatSumTo 1 i xs     = if (elem i xs) then Just [i] else Nothing
nThatSumTo 2 i xs     = (\(a, b) -> [a, b]) <$> twoThatSumTo i xs
nThatSumTo n i (x:xs) = (x :) <$> (nThatSumTo (n - 1) (i - x) xs) <|> nThatSumTo n i xs

-- The problems

runProblem :: ([ Int ] -> a) -> IO (Either String a)
runProblem = runFile' "data/day01/0101.txt" (onePerLine AP.decimal)

star1 :: IO (Either String (Maybe Int))
star1 = runProblem 
  ( fmap (\(a, b) -> a * b) 
  . twoThatSumTo 2020 
  . sort)

star2 :: IO (Either String (Maybe Int))
star2 = runProblem 
  ( fmap (\(a, b, c) -> a * b * c) 
  . threeThatSumTo 2020 
  . sort
  )

-- Show the answers rather than multiply them
runNSum :: Int -> Int -> IO (Either String (Maybe [ Int ]))
runNSum n i = runProblem (nThatSumTo n i . sort)