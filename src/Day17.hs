module Day17 (star1, star2, runCycles, runCycles4, realBoard, realBoard4) where

import Data.List (delete)
import Data.Set (Set)
import qualified Data.Set as S

type Loc = (Int, Int, Int)
type Loc4 = (Int, Int, Int, Int)

type Board a = Set a

extend2 :: c -> (a, b) -> (a, b, c)
extend2 z (x, y) = (x, y, z)

extend3 :: d -> (a, b, c) -> (a, b, c, d)
extend3 w (x, y, z) = (x, y, z, w)

cube :: Loc -> [ Loc ]
cube (x, y, z) = do
  zz <- [ z - 1, z, z + 1]
  xx <- [ x - 1, x, x + 1]
  yy <- [ y - 1, y, y + 1]
  return (xx, yy, zz)

hypercube :: Loc4 -> [ Loc4 ]
hypercube (x, y, z, w) = do
  (xx, yy, zz) <- cube (x, y, z)
  ww           <- [w - 1, w, w + 1]
  return (xx, yy, zz, ww)

neighbours3 :: Loc -> Set Loc
neighbours3 loc = S.fromList (delete loc (cube loc))

neighbours4 :: Loc4 -> Set Loc4
neighbours4 loc = S.fromList (delete loc (hypercube loc))

countIntersection :: Ord a => Set a -> Set a -> Int
countIntersection b1 b2 = S.size (S.intersection b1 b2)

processNeighbour :: Ord a => (a -> Set a) -> Set a -> a -> Set a
processNeighbour neighbourFn board loc =
  let
    countNeighbours = countIntersection (neighbourFn loc) board 
  in
    if S.notMember loc board && countNeighbours == 3 then
      S.singleton loc
    else
      S.empty

doStuff :: Ord a => (a -> Set a) -> Board a -> a -> (Board a, Set a)
doStuff neighbourfn board loc =
  let 
    neighbours = neighbourfn loc
    occupiedNeighbours = countIntersection neighbours board
  in
    if (occupiedNeighbours < 2 || occupiedNeighbours > 3) then
      (S.empty, neighbours)
    else
      (S.singleton loc, neighbours)

stepBoard :: Ord a => (a -> Set a) -> Board a -> Board a
stepBoard neighbourfn startingBoard =
  let
    (updatedOccupiedLocs, foundNeighbours) = S.foldr'
      (\loc (newBoard, newNeighbours) ->
        case (doStuff neighbourfn startingBoard loc) of
          (board, neighbours) -> (newBoard <> board, newNeighbours <> neighbours)) 
      (S.empty, S.empty) 
      startingBoard
    processNeighbours ns =
      S.foldr' (\l b -> b <> processNeighbour neighbourfn startingBoard l) S.empty ns
  in
    updatedOccupiedLocs <> processNeighbours foundNeighbours

realBoard :: Set Loc
realBoard = (S.fromList . (fmap (extend2 0))) 
 [ (0, 0), (1, 0),                         (5, 0),         (7, 0)
 , (0, 1), (1, 1), (2, 1), (3, 1),         (5, 1),         (7, 1)
 , (0, 2),                         (4, 2), (5, 2), (6, 2), (7, 2)
 ,                 (2, 3),         (4, 3),         (6, 3)
 , (0, 4), (1, 4), (2, 4), (3, 4),         (5, 4)
 , (0, 5),         (2, 5),         (4, 5),                 (7, 5)
 ,         (1, 6), (2, 6), (3, 6), (4, 6),         (6, 6), (7, 6)
 ,                 (2, 7),                         (6, 7), (7, 7)
 ]

realBoard4 :: Set Loc4
realBoard4 = S.map (extend3 0) realBoard

runCycles :: Board Loc -> Int -> Board Loc
runCycles board i = applyN i (stepBoard neighbours3) board

runCycles4 :: Board Loc4 -> Int -> Board Loc4
runCycles4 board i = applyN i (stepBoard neighbours4) board

applyN :: Int -> (a -> a) -> a -> a
applyN i f a = foldr (const f) a [1..i]

star1 :: Int
star1 = S.size (runCycles realBoard 6)

star2 :: Int
star2 = S.size (runCycles4 realBoard4 6)