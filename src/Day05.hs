module Day05 (star1, star2) where

import           Prelude                      hiding (Left, Right)
    
import qualified Data.Attoparsec.ByteString.Char8 as AP

import           Data.List                           (find, sort)
import           Lib                                 (safeHead, runFile',
                                                     onePerLine)

type Range        = (Int, Int)
data SeatRange    = SeatRange Range Range
data Partition    = Front | Back | Left | Right
type BoardingPass = [ Partition ]

partition :: Partition -> SeatRange -> SeatRange
partition p (SeatRange r s) = 
  let 
    halve l h = l + (h - l) `div` 2
    lowerHalf (l, h) = (l            , halve l h)
    upperHalf (l, h) = (halve l h + 1, h        ) 
  in case p of
    Front -> SeatRange (lowerHalf r) s
    Back  -> SeatRange (upperHalf r) s
    Left  -> SeatRange r             (lowerHalf s)
    Right -> SeatRange r             (upperHalf s)

seatNumbers :: [ BoardingPass ] -> Maybe [ Int ]
seatNumbers ps = sequence (fmap (seatNumber . findSeat) ps)

findSeat :: BoardingPass -> SeatRange
findSeat = foldl (flip partition) (SeatRange (0, 127) (0, 7))

seatNumber :: SeatRange -> Maybe Int
seatNumber (SeatRange row seat)
  =   (\r s -> r * 8 + s) 
  <$> collapse row
  <*> collapse seat

findFirstMissing :: (Ord a, Enum a) => [ a ] -> Maybe a
findFirstMissing ss = do
  low <- safeHead ss
  fst <$> (find (\(a, b) -> a /= b) 
                (zip [low..] (sort (filter (>= low) ss))))

collapse :: Eq a => (a, a) -> Maybe a
collapse (a, b)
  | a == b    = Just a
  | otherwise = Nothing

-- Parsers

parseBoardingPass :: AP.Parser BoardingPass
parseBoardingPass = 
  let parsePartition (c, f) = const f <$> AP.char c
  in AP.many1
    ( AP.choice
      ( fmap parsePartition 
        [ ('F', Front), ('B', Back), ('L', Left), ('R', Right) ]
      )
    )

parseBoardingPasses :: AP.Parser [ BoardingPass ]
parseBoardingPasses = onePerLine parseBoardingPass

-- Puzzles

runProblem :: ([ BoardingPass ] -> a) -> IO (Either String a)
runProblem = runFile' "data/day05/0501.txt" parseBoardingPasses

star1 :: IO (Either String (Maybe Int))
star1 = runProblem (\ps -> fmap maximum (seatNumbers ps))

star2 :: IO (Either String (Maybe Int))
star2 = runProblem (\ps -> (findFirstMissing) =<< (seatNumbers ps))
