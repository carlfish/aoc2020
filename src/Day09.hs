module Day09 where
import qualified Data.Attoparsec.ByteString.Char8 as AP

import qualified Data.Vector as V
import Data.Vector ((!))
import Data.Maybe (isJust)
import Day01 (twoThatSumTo)
import Data.List (sort)
import Lib (runFile', onePerLine)

findBadNumber :: Int -> [ Int ] -> Maybe Int
findBadNumber preambleLength numbers =
  let
    preamble = take preambleLength numbers
    is       = drop preambleLength numbers
  in
    snd $ foldl (\(seen, r) n -> 
        if isJust r then (seen, r)
        else (if (isJust (twoThatSumTo n (sort seen))) then
          ((drop 1 seen) <> [ n ], Nothing)
          else (seen, Just n))
      ) (preamble, Nothing) is

-- Uses unsafe vector access for code clarity
unsafeFindContiguousSum :: Int -> [ Int ] -> [ Int ]
unsafeFindContiguousSum target is =
  let
    numbers = V.fromList is
    sliceNumbers tptr hptr = V.toList (V.slice tptr (hptr - tptr + 1) numbers)
    tryNext sumSoFar tptr hptr = 
      let h = numbers ! hptr
          t = numbers ! tptr
      in
        case (compare (sumSoFar + h) target) of
          EQ -> sliceNumbers tptr hptr
          LT -> tryNext (sumSoFar + h) tptr (hptr + 1)
          GT -> tryNext (sumSoFar - t) (tptr + 1) hptr
  in
    tryNext (numbers ! 0) 0 1

runProblem :: ([Int] -> a) -> IO (Either String a)
runProblem f = runFile' "data/day09/0901.txt" (onePerLine AP.decimal) f

star1 :: IO (Either String (Maybe Int))
star1 = runProblem (findBadNumber 25)

star2 :: IO (Either String Int)
star2 = runProblem (\is ->
    let result = unsafeFindContiguousSum 31161678 is
    in (minimum result + maximum result)
  )