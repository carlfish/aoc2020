module Day02 (star1, star2) where

import qualified Data.Attoparsec.ByteString.Char8 as AP
import qualified Data.ByteString.Char8            as BS

import           Data.Bits                        (xor)
import           Data.ByteString.Char8            (ByteString)
import           Data.Maybe                       (fromMaybe)

import           Lib                              (bsIndex, count,
                                                   onePerLine, runFile')

data Range = Range { rmin :: Int, rmax :: Int}
data Rule  = Rule Range Char

inRange :: Range -> Int -> Bool
inRange r i = (i >= rmin r) && (i <= rmax r)

matchesRule :: Rule -> ByteString -> Bool
matchesRule (Rule range c) password = 
    inRange range ((BS.length . (BS.filter (==c))) password)

matchesRule2 :: Rule -> ByteString -> Bool
matchesRule2 (Rule range c) s = 
  let
    charIsAt i = (==c) <$> bsIndex s (i - 1)
  in
    fromMaybe False (xor <$> (charIsAt (rmin range)) <*> (charIsAt (rmax range)))

-- Parser

parseLine :: AP.Parser (Rule, ByteString)
parseLine =
  let
    parseRange = Range <$> AP.decimal <* AP.char '-' <*> AP.decimal
    parseRule  = Rule  <$> parseRange <* AP.space <*> AP.anyChar
  in
    (,) <$> parseRule <* AP.char ':' <*> (AP.space *> AP.takeWhile AP.isAlpha_ascii)

-- Run Problem 

runProblem :: ([ (Rule, ByteString) ] -> a) -> IO (Either String a)
runProblem = runFile' "data/day02/0201.txt" (onePerLine parseLine)

star1 :: IO (Either String Int)
star1 = runProblem (count id .
 fmap (\(rule, password) -> matchesRule rule password))

star2 :: IO (Either String Int)
star2 = runProblem (count id .
 fmap (\(rule, password) -> matchesRule2 rule password))