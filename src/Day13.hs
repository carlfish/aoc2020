module Day13 (star1, star2) where

import qualified Data.Attoparsec.ByteString.Char8 as AP
import           Control.Applicative              (Alternative((<|>)))
import Data.List (find)
import Lib (runFile')
import Data.Int (Int64)
 
data Bus = Unsched | Sched Int64 
data Input = Input
  { startTime :: Int64
  , buses     :: [ Bus ]
  }

parseBus :: AP.Parser Bus
parseBus = (Sched <$> AP.decimal) <|> (const Unsched <$> AP.char 'x')

busNumber :: Bus -> Maybe Int64
busNumber Unsched = Nothing
busNumber (Sched i) = Just i

parseInput :: AP.Parser Input
parseInput = Input <$> AP.decimal <* AP.endOfLine <*> AP.sepBy parseBus (AP.char ',') 

busAt :: Int64 -> Bus -> Bool
busAt _ Unsched = False
busAt i (Sched n) = i `mod` n == 0

findFirstBus :: Input -> Maybe (Int64, Bus)
findFirstBus input =
  foldr (\t b -> ((t,) <$> find (busAt t) (buses input)) <|> b) Nothing [(startTime input)..]

-- The Chinese Remainder Theorem is faster, but this is simpler, my own work, and I understand what
-- it's doing.

toDivisors :: [ Bus ] -> [ (Int64, Int64)]
toDivisors bs = 
  foldl (\l (i, b) -> maybe l (\n -> (i, n) : l) (busNumber b)) [] (zip [0..] bs)

findMatch :: [ (Int64, Int64) ] -> Int64
findMatch is =
  let 
    findMatch' i step indexedbuses =
      case indexedbuses of
        [] -> i
        (busindex, busnumber):rest -> 
          if (i + step + busindex) `mod` busnumber == 0 
            then findMatch' (i + step) (lcm step busnumber) rest
            else findMatch' (i + step) step indexedbuses
  in
    findMatch' 0 1 is

-- Run puzzles

runProblem' :: (Input -> a) -> IO (Either String a)
runProblem' = runFile' "data/day13/1301.txt" parseInput

star1 :: IO (Either String (Maybe Int64))
star1 = 
  let 
    answer input = do
      (t, bus) <- findFirstBus input
      n <- busNumber bus
      return ((t - startTime input) * n)
  in runProblem' answer

star2 :: IO (Either String Int64)
star2 = runProblem' (findMatch . toDivisors . buses)