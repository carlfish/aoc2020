module Day04 (star1, star2) where
 
import Prelude                   hiding (takeWhile, null, drop)

import Data.Attoparsec.ByteString.Char8

import Control.Applicative              (Alternative((<|>)))
import Data.ByteString.Char8            (ByteString)
import Lib (runFileBlankLineSeparated)

type FieldName = ByteString

requiredFields :: [ FieldName ]
requiredFields = [ "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid" ]

isValid :: [ FieldName ] -> Bool
isValid passport =
  let
    containsAllOf _ [] = True
    containsAllOf fs (rf: rfs) = elem rf fs && containsAllOf fs rfs
  in
    containsAllOf passport requiredFields 

numParser :: Int -> Int -> Parser Int
numParser low high = do
  y <- decimal
  if (y < low || y > high) then fail "out of range"
  else pure y

fieldParser :: FieldName -> Parser a -> Parser FieldName
fieldParser fName p = string fName <* char ':' <* p

byrParser, iyrParser, eyrParser, hgtParser, hclParser, eclParser, pidParser, cidParser 
 :: Parser ByteString

byrParser = fieldParser "byr" (numParser 1920 2002)
iyrParser = fieldParser "iyr" (numParser 2010 2020)
eyrParser = fieldParser "eyr" (numParser 2020 2030)
hgtParser = fieldParser "hgt" ((numParser 150 193 <* string "cm") <|> (numParser 59 76 <* string "in"))
hclParser = fieldParser "hcl" (char '#' *> takeWhile (inClass "a-f0-9"))
eclParser = fieldParser "ecl" (choice (string <$> ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]))
pidParser = fieldParser "pid" (count 9 digit)
cidParser = fieldParser "cid" pAlphaNum

pAlphaNum :: Parser ByteString
pAlphaNum = takeWhile (inClass "#a-zA-Z0-9")

pField :: Parser FieldName
pField = takeWhile isAlpha_ascii <* char ':' <* pAlphaNum

pValidField :: Parser FieldName
pValidField =
  byrParser <|> iyrParser <|> eyrParser <|> hgtParser <|> hclParser <|> eclParser <|> pidParser <|> cidParser

pFields :: Parser FieldName -> Parser [ FieldName ]
pFields p = many1 (p <* ((const () <$> space) <|> endOfLine <|> endOfInput))

-- Run the problem

runProblem' :: Parser [ FieldName ] -> ([[ FieldName ]] -> a) -> IO a
runProblem' ps f = runFileBlankLineSeparated "data/day04/0401.txt" ps f

star1 :: IO Int
star1 = runProblem' (pFields pField) (length . filter isValid)

star2 :: IO Int
star2 = runProblem' (pFields pValidField) (length . filter isValid)