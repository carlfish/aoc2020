module Lib 
    ( parseFile
    , onePerLine
    , whitespace
    , commaSeparated
    , runFile
    , runFile'
    , runFileBlankLineSeparated
    , runFileIO
    , bsIndex
    , count
    , safeHead
    , testParser
    ) where

import qualified Data.Attoparsec.ByteString.Char8 as AP

import Data.Attoparsec.ByteString.Char8 (Parser)
import System.IO (openFile, IOMode(..))
import Data.ByteString (hGetContents)
import Data.ByteString (ByteString)
import qualified Data.ByteString.Char8 as BC
import Data.Maybe (listToMaybe)
import Data.Either (rights)

-- Missing from this version of ByteString

bsIndex :: ByteString -> Int -> Maybe Char
bsIndex bs i = 
  let l = (BC.length bs)
  in if (i < 0  || i >= l) then Nothing
     else Just (BC.index bs i) 

-- Helpers

count :: (a -> Bool) -> [ a ] -> Int
count f = length . filter f

safeHead :: [ a ] -> Maybe a
safeHead = listToMaybe

-- Parsers

onePerLine :: Parser a ->Parser [ a ]
onePerLine p = AP.many' (p <* AP.choice [AP.endOfLine, AP.endOfInput])

whitespace :: AP.Parser ByteString
whitespace = AP.takeWhile AP.isSpace

commaSeparated :: Parser a ->Parser [ a ]
commaSeparated p = AP.sepBy p (AP.char ',' <* whitespace)

loadFile :: FilePath -> IO ByteString
loadFile file = do
  handle <- openFile file ReadMode
  hGetContents handle

parseFile :: String -> Parser a -> IO (Either String a)
parseFile file parser = AP.parseOnly parser <$> loadFile file 

splitFileOnBlankLines :: String -> IO [ ByteString ]
splitFileOnBlankLines file = 
  let
    splitOnBlanks bs = 
        h : if BC.null t then [] else splitOnBlanks (BC.drop 2 t)
             where (h,t) = BC.breakSubstring "\n\n" bs
  in 
    splitOnBlanks <$> loadFile file

testParser :: Parser a -> ByteString -> Either String a
testParser p s = AP.parseOnly p s

-- Exercise runners

runFileIO :: String -> Parser a -> (a -> IO (Either String b)) -> IO (Either String b)
runFileIO file parser processor = do
  p <- parseFile file parser
  either (return . Left) processor p

runFile :: String -> Parser a -> (a -> Either String b) -> IO (Either String b)
runFile file parser processor = ((=<<) processor) <$> parseFile file parser

runFile' :: String -> Parser a -> (a -> b) -> IO (Either String b)
runFile' file parser processor = runFile file parser (pure . processor)

runFileBlankLineSeparated :: String -> Parser a -> ([a] -> b) -> IO b
runFileBlankLineSeparated file parser processor = 
  let parseSections = (processor . rights . (fmap (AP.parseOnly parser)))
  in parseSections <$> splitFileOnBlankLines file