module Day11 where

import qualified Data.Matrix                      as M
import qualified Data.Attoparsec.ByteString.Char8 as AP
import Lib (onePerLine, runFile')
import           Control.Applicative              (Alternative((<|>)))
import Data.Maybe (fromMaybe)

data Loc = Floor | Empty | Occupied | OutOfBounds
  deriving (Eq, Show)

newtype Grid = Grid (M.Matrix Loc)
  deriving (Eq, Show)

type NeighbourFinder = Int -> Int -> (Int -> Int) -> (Int -> Int) -> Grid -> Loc
type SeatingRule = Loc -> Int -> Loc

-- The seating grid is surrounded by lava
elemAt :: Int -> Int -> Grid -> Loc
elemAt row col (Grid m) = fromMaybe OutOfBounds (M.safeGet row col m)

occupants :: Loc -> Int
occupants Occupied = 1
occupants _        = 0

countNeighbours :: NeighbourFinder -> Int -> Int -> Grid -> Int
countNeighbours neighbourFinder row col grid =
  let
    ns stepr stepc = neighbourFinder row col stepr stepc grid
  in
      occupants (ns (subtract 1) (subtract 1)) 
    + occupants (ns (subtract 1) id)
    + occupants (ns (subtract 1) ((+) 1))
    + occupants (ns (id)         (subtract 1)) 
    + occupants (ns (id)         ((+) 1))
    + occupants (ns ((+) 1)      (subtract 1)) 
    + occupants (ns ((+) 1)      id)
    + occupants (ns ((+) 1)      ((+) 1))

firstAdjacent :: NeighbourFinder
firstAdjacent row col stepRow stepCol grid =
  elemAt (stepRow row) (stepCol col) grid

firstVisible :: NeighbourFinder
firstVisible row col stepRow stepCol grid =
  let
    row' = stepRow row
    col' = stepCol col
    l = elemAt row' col' grid
  in
    case l of 
      Floor -> firstVisible row' col' stepRow stepCol grid
      _     -> l

adjacentNeighboursRule :: SeatingRule
adjacentNeighboursRule Empty occ    = if occ == 0 then Occupied else Empty
adjacentNeighboursRule Occupied occ = if occ >= 4 then Empty else Occupied
adjacentNeighboursRule l _      = l

visibleNeighboursRule :: SeatingRule
visibleNeighboursRule Empty occ    = if occ == 0 then Occupied else Empty
visibleNeighboursRule Occupied occ = if occ >= 5 then Empty else Occupied
visibleNeighboursRule l _      = l

coordsMatrix :: Int -> Int -> M.Matrix (Int, Int)
coordsMatrix rows cols =
  if (rows == 97 && cols == 97) then cheatCoordsMatrix
  else coordsMatrix' rows cols

cheatCoordsMatrix :: M.Matrix (Int, Int)
cheatCoordsMatrix = coordsMatrix' 97 97

coordsMatrix' :: Int -> Int -> M.Matrix (Int, Int)
coordsMatrix' rows cols =
  M.fromLists $ do
    i <- [1 .. (cols)]
    pure (zip (repeat i) [1..rows])

fromLists :: [[ Loc ]] -> Grid
fromLists = Grid . M.fromLists

stepGrid :: (Loc -> Int -> Loc) -> NeighbourFinder -> Grid -> Grid
stepGrid decision neighbourFinder g@(Grid m) = Grid $ M.elementwise (\l (r, c) -> decision l (countNeighbours neighbourFinder r c g)) m (coordsMatrix (M.nrows m) (M.ncols m))

countOccupants :: Grid -> Int
countOccupants (Grid m) = foldr (\l s -> s + occupants l) 0 m

parseLoc :: AP.Parser Loc
parseLoc = (const Floor <$> AP.char '.') <|> (const Empty <$> AP.char 'L') <|> (const Occupied <$> AP.char '#')

parseLine :: AP.Parser [ Loc ]
parseLine = AP.many1 parseLoc

parseGrid :: AP.Parser Grid
parseGrid = fromLists <$> (onePerLine parseLine)

fixed :: Eq a => (a -> a) -> a -> a
fixed f a 
  | a == a' = a
  | otherwise = fixed f a'
  where a' = f a

runProblem :: (Grid -> a) -> IO (Either String a)
runProblem = runFile' "data/day11/1101.txt" parseGrid

star1 :: IO (Either String Int)
star1 = runProblem (countOccupants . fixed (stepGrid adjacentNeighboursRule firstAdjacent))

star2 :: IO (Either String Int)
star2 = runProblem (countOccupants . fixed (stepGrid visibleNeighboursRule firstVisible))