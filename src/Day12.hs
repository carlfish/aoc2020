module Day12 where

import qualified Data.Attoparsec.ByteString.Char8 as AP
import Lib (runFile', onePerLine)

data Command = Dir Facing | L | R | F
data Facing = N | S | E | W
data Action = Action Command Int 
data Location = Location 
  { xpos   :: Int 
  , ypos   :: Int
  }

moveBy :: Int -> Int -> Location -> Location
moveBy x y (Location lx ly) = Location (x + lx) (y + ly)

move :: Facing -> Int -> Location -> Location
move f i = case f of
  N -> moveBy   0   i
  S -> moveBy   0 (-i)
  E -> moveBy   i   0
  W -> moveBy (-i)  0

each90Degrees :: (a -> a) -> Int -> a -> a
each90Degrees _ 0 l = l
each90Degrees step degrees l
  | (degrees `mod` 90 == 0) = each90Degrees step (degrees - 90) (step l)
  | otherwise = error "Can only rotate right angles"

-- Star 1 (no waypoint)

data Ship = Ship 
  { facing   :: Facing 
  , location :: Location
  }

left :: Facing -> Facing
left N = W
left W = S
left S = E
left E = N

right :: Facing -> Facing
right N = E
right E = S
right S = W
right W = N

act :: Action -> Ship -> Ship
act (Action cmd i) = 
  let 
    moveShip f (Ship ff l) = Ship ff (move f i l)
    turnTo turn (Ship f l) = Ship (turn f) l
  in case cmd of
    Dir f -> moveShip f
    L     -> each90Degrees (turnTo left) i
    R     -> each90Degrees (turnTo right) i
    F     -> \s -> act (Action (Dir (facing s)) i) s

-- Star 2 (with waypoint)

data Locations = Locations
  { ship     :: Location -- absolute
  , waypoint :: Location -- relative to ship
  }

modifyWaypoint :: (Location -> Location) -> Locations -> Locations
modifyWaypoint f ls = ls { waypoint = f (waypoint ls) }

leftAroundOrigin :: Location -> Location
leftAroundOrigin (Location x y) = Location (-y) x

rightAroundOrigin :: Location -> Location
rightAroundOrigin (Location x y) = Location y (-x)

actW :: Action -> Locations -> Locations
actW (Action cmd i) = 
  let
    moveWaypoint f = modifyWaypoint (move f i)
    moveTowardsWaypoint n (Locations s w) = 
      Locations (moveBy (n * xpos w) (n * ypos w) s) w
  in case cmd of
    Dir f -> moveWaypoint f
    L     -> modifyWaypoint (each90Degrees leftAroundOrigin i)
    R     -> modifyWaypoint (each90Degrees rightAroundOrigin i)
    F     -> moveTowardsWaypoint i

-- Parsers

parseAction :: AP.Parser Action
parseAction =
  let
     parseAnyCmd (c, f) = const f <$> AP.char c
     parseCmd = AP.choice
       (fmap parseAnyCmd [('N', Dir N), ('S', Dir S), ('E', Dir E), 
                         ('W', Dir W), ('L', L), ('R', R), ('F', F)])      
  in
    Action <$> parseCmd <*> AP.decimal

parseActions :: AP.Parser [ Action ]
parseActions = onePerLine parseAction

-- The Puzzle

origin :: Location
origin = Location 0 0

shipStart :: Ship
shipStart = Ship E origin

waypointStart :: Location
waypointStart = Location 10 1

manhattanDistanceFromOrigin :: Location -> Int
manhattanDistanceFromOrigin (Location x y) = (abs x) + (abs y)

runProblem :: ([Action] -> a) -> IO (Either String a)
runProblem = runFile' "data/day12/1201.txt" parseActions

star1 :: IO (Either String Int)
star1 = runProblem (manhattanDistanceFromOrigin . location . foldl (flip act) shipStart)

star2 :: IO (Either String Int)
star2 = runProblem 
  ( manhattanDistanceFromOrigin 
  . ship 
  . foldl (flip actW) (Locations origin waypointStart)
  )