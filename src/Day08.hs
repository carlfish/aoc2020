module Day08 where

import           Data.Attoparsec.ByteString.Char8
import Data.Vector (Vector, (!?), fromList, ifoldl, (//))
import Data.Set (Set, empty, insert)
import Control.Applicative (Alternative((<|>)))
import Lib ( onePerLine, runFile )
import Control.Monad.Except (throwError)
import Control.Monad.State (evalStateT, StateT, get, modify)
import Data.Bifunctor (Bifunctor(bimap))

data Inst = Nop Int | Acc Int | Jmp Int
type Instructions = Vector Inst

data MState = MState { acc :: Int, iptr :: Int, visited :: Set Int }

data Err = Looped Int | OutOfBounds Int | NoProgramFound
  deriving Show

type Program a = StateT MState (Either Err) a

initState :: MState
initState = MState 0 0 empty

run :: Vector Inst -> Either Err Int
run insts =
  let 
    updateVisited p = modify (\s -> s{ visited = (insert p (visited s)) })
    incAcc i =        modify (\s -> s{ acc = (acc s) + i })
    incPtr i =        modify (\s -> s{ iptr = (iptr s) + i })
    run' = do
      (MState a p v) <- get
      if (elem p v) then throwError (Looped a)
      else if (p == length insts) then return a
      else maybe
         (throwError (OutOfBounds p))
         (\inst -> updateVisited p >> runInstruction inst >> run')
         (insts !? p)

    runInstruction = \case
      Nop _ -> incPtr 1
      Acc i -> incPtr 1 >> incAcc i
      Jmp i -> incPtr i

  in
    evalStateT run' initState

tryPrograms :: Vector Inst -> Either Err Int
tryPrograms program =
  ifoldl (\r idx -> \case
              Nop i -> run (program // [(idx, Jmp i)]) `orElse` r
              Jmp i -> run (program // [(idx, Nop i)]) `orElse` r
              Acc _ -> r
          )
         (Left NoProgramFound)
         program 

orElse :: Either a b -> Either a b -> Either a b
orElse (Left _) x    = x
orElse x@(Right _) _ = x

-- Parsers 

parseOpcode :: Parser (Int -> Inst)
parseOpcode 
  =   (const Nop <$> string "nop") 
  <|> (const Acc <$> string "acc") 
  <|> (const Jmp <$> string "jmp")

parseInst :: Parser Inst
parseInst = parseOpcode <* space <*> (signed decimal)

parseProgram :: Parser (Vector Inst)
parseProgram = fromList <$> onePerLine parseInst

-- Puzzles

runProblem :: ((Vector Inst) -> Either Err a) -> IO (Either String a)
runProblem f = runFile "data/day08/0801.txt" parseProgram ((bimap show id) . f)

star1 :: IO (Either String Int)
star1 = runProblem run

star2 :: IO (Either String Int)
star2 = runProblem tryPrograms