module Day15 (star1, star2, runPuzzle) where

import Prelude hiding (replicate)
import Data.Array.MArray (MArray, newArray, readArray, writeArray)
import Data.Array.ST (STUArray)
import Control.Monad.ST (ST, runST)

nextTurn :: MArray a Int m => Int -> Int -> a Int Int -> m Int
nextTurn i turn st = 
  (\case
    0 -> 0
    n -> turn - n
  ) 
  <$> readArray st i

stArray :: Int -> ST s (STUArray s Int Int)
stArray capacity = newArray (0, capacity) 0

runGame :: [ Int ] -> Int -> Int
runGame startingNumbers turns =
  let
    maxIdx = maximum ((turns + 1):startingNumbers)
    runGame' turn lastNumber remainingStartingNumbers state =
      if (turn == turns) then
        return lastNumber
      else
        case remainingStartingNumbers of
          [] -> do
            nextNumber <- nextTurn lastNumber turn state
            writeArray state lastNumber turn
            runGame' (turn + 1) nextNumber [] state
          nextNumber:ns -> do
            writeArray state lastNumber turn
            runGame' (turn + 1) nextNumber ns state
  in
    runST ( stArray maxIdx >>= (runGame' 0 0 startingNumbers))

puzzleInput :: [ Int ]
puzzleInput = [12,20,0,6,1,17,7]

runPuzzle :: Int -> Int
runPuzzle turns = runGame puzzleInput turns

star1 :: Int
star1 = runGame puzzleInput 2020

star2 :: Int
star2 = runGame puzzleInput 30000000