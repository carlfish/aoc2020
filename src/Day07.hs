module Day07 where

import Prelude hiding (takeWhile)
import           Data.Attoparsec.ByteString.Char8
import           Data.ByteString.Char8            (ByteString)
import Control.Applicative (Alternative((<|>)))
import Lib (runFile', onePerLine)
import qualified Data.Map.Strict as M
import qualified Data.Set as S
import Data.Maybe (fromMaybe)

newtype Color     = Color ByteString
  deriving (Eq, Ord, Show)
newtype Adjective = Adjective ByteString
  deriving (Eq, Ord, Show)

data Bag = Bag Adjective Color
  deriving (Eq, Ord)
instance Show Bag where
  show (Bag (Adjective a) (Color c)) = show ( a <> " " <> c <> " bag")

type Contains = (Int, Bag)

data Rule = Rule Bag [ Contains ]
  deriving (Eq, Ord, Show)

type Ruleset        = M.Map Bag [ Contains ]
type InverseRuleset = M.Map Bag (S.Set Bag)


makeRuleset :: [ Rule ] -> Ruleset
makeRuleset = M.fromList . fmap (\(Rule b cs) -> (b, cs))

makeInverseRuleset :: [ Rule ] -> InverseRuleset
makeInverseRuleset = 
  foldr 
    ( \(Rule bag contains) m -> 
        foldr (
          \(_, b) -> M.insertWith S.union b (S.singleton bag)
        ) m contains
    ) M.empty

findReachableFrom :: Bag -> InverseRuleset -> S.Set Bag
findReachableFrom bag rules =
   let 
     candidates = fromMaybe S.empty (M.lookup bag rules)
   in
     S.foldr (\b s -> S.union s (findReachableFrom b rules)) candidates candidates

countInside :: Bag -> Ruleset -> Int
countInside bag rules =
   let 
     candidates = fromMaybe [] (M.lookup bag rules)
   in
     sum (fst <$> candidates) +
       (sum ((\(i, b) -> i * countInside b rules) <$> candidates))

-- The Parser

parseBag :: Parser Bag
parseBag = 
  let
    parseAdj = Adjective <$> takeWhile isAlpha_ascii
    parseCol = Color <$> takeWhile isAlpha_ascii
  in
    Bag <$> parseAdj <* space <*> parseCol <* space <* (string "bags" <|> string "bag")

parseContains :: Parser [ Contains ]
parseContains = 
  let
    parseEmpty = const [] <$> string "no other bags" 
    parseSingleContain = (,) <$> decimal <* space <*> parseBag
  in
    (parseEmpty <|> sepBy parseSingleContain ", ") <* char '.'

parseRule :: Parser Rule
parseRule = Rule <$> parseBag <* space <* string "contain" <* space <*> parseContains

parseRules :: Parser [ Rule ]
parseRules = onePerLine parseRule

-- The Puzzle

testBag :: Bag
testBag = (Bag (Adjective "shiny") (Color "gold"))

runProblem :: ([ Rule ] -> a) -> IO (Either String a)
runProblem = runFile' "data/day07/0701.txt" parseRules

star1 :: IO (Either String Int)
star1 = runProblem (S.size . findReachableFrom testBag . makeInverseRuleset)

star2 :: IO (Either String Int)
star2 = runProblem (countInside testBag . makeRuleset)