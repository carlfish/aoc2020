module Day03 (star1, star2) where

import qualified Data.Attoparsec.ByteString.Char8 as AP
import qualified Data.Matrix                      as M

import           Control.Applicative              (Alternative((<|>)))
import           Data.Ratio                       ((%), Ratio, numerator, 
                                                   denominator)

import           Lib                              (count, runFile',
                                                   onePerLine)

data Square  = Open | Tree
newtype Grid = Grid (M.Matrix Square)
type Slope   = Ratio Int

path :: Slope -> Grid -> [ Square ]
path slope grid =
  let 
    _path x y =
        maybe 
          [] 
          (: _path (x + numerator slope) (y + denominator slope)) 
          (gridLookup x y grid)
  in
    _path 0 0

-- zero-based x/y get with wraparound on x axis
gridLookup :: Int -> Int -> Grid -> Maybe Square
gridLookup x y (Grid m) = M.safeGet (y + 1) ((x `mod` M.ncols m) + 1) m

fromLists :: [[ Square ]] -> Grid
fromLists = Grid . M.fromLists

isTree :: Square -> Bool
isTree Tree = True
isTree _    = False

multiply :: Foldable f => Integral i => f i -> i
multiply = foldr (*) 1

-- Parser

parseSquare :: AP.Parser Square
parseSquare = (const Open <$> AP.char '.') <|> (const Tree <$> AP.char '#')

parseLine :: AP.Parser [ Square ]
parseLine = AP.many1 parseSquare

parseGrid :: AP.Parser Grid
parseGrid = fromLists <$> (onePerLine parseLine)

-- Puzzles

runProblem :: (Grid -> a) -> IO (Either String a)
runProblem = runFile' "data/day03/0301.txt" parseGrid

star1 :: IO (Either String Int)
star1 = runProblem (count isTree . path (3 % 1))

star2 :: IO (Either String Int)
star2 = runProblem 
  ( \grid -> 
    multiply ( fmap 
                (count isTree . (\slope -> path slope grid)) 
                [1%1, 3%1, 5%1, 7%1, 1%2]
             )
  )
